﻿using DevExpress.Data;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GridMultipleLevel.Test3
{
    public class Item
    {
        public int Id { get; set; }
        public int ParentId { get; set; }
        public string Name { get; set; }

        public Item(int iId, int iParentId, string strName)
        {
            Id = iId;
            ParentId = iParentId;
            Name = strName;
        }
    }
}
