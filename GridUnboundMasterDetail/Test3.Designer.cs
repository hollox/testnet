﻿namespace GridMultipleLevel.Test3
{
    partial class Test3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode2 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode3 = new DevExpress.XtraGrid.GridLevelNode();
            this.oGridViewLevel1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.level2colId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.level2colName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.oGridControl = new DevExpress.XtraGrid.GridControl();
            this.oGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.level1colId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.level1colName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.oGridViewLevel2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.level3colId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.level3colName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.oGridViewLevel3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.level4colId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.level4colName = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.oGridViewLevel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.oGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.oGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.oGridViewLevel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.oGridViewLevel3)).BeginInit();
            this.SuspendLayout();
            // 
            // oGridViewLevel1
            // 
            this.oGridViewLevel1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.level2colId,
            this.level2colName});
            this.oGridViewLevel1.GridControl = this.oGridControl;
            this.oGridViewLevel1.Name = "oGridViewLevel1";
            this.oGridViewLevel1.MasterRowEmpty += new DevExpress.XtraGrid.Views.Grid.MasterRowEmptyEventHandler(this.oGridViewLevel1_MasterRowEmpty);
            this.oGridViewLevel1.MasterRowGetChildList += new DevExpress.XtraGrid.Views.Grid.MasterRowGetChildListEventHandler(this.oGridViewLevel1_MasterRowGetChildList);
            this.oGridViewLevel1.MasterRowGetRelationName += new DevExpress.XtraGrid.Views.Grid.MasterRowGetRelationNameEventHandler(this.oGridViewLevel1_MasterRowGetRelationName);
            this.oGridViewLevel1.MasterRowGetRelationCount += new DevExpress.XtraGrid.Views.Grid.MasterRowGetRelationCountEventHandler(this.oGridViewLevel1_MasterRowGetRelationCount);
            // 
            // level2colId
            // 
            this.level2colId.Caption = "Id";
            this.level2colId.FieldName = "Id";
            this.level2colId.Name = "level2colId";
            this.level2colId.Visible = true;
            this.level2colId.VisibleIndex = 0;
            // 
            // level2colName
            // 
            this.level2colName.Caption = "Name";
            this.level2colName.FieldName = "Name";
            this.level2colName.Name = "level2colName";
            this.level2colName.Visible = true;
            this.level2colName.VisibleIndex = 1;
            // 
            // oGridControl
            // 
            this.oGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode1.LevelTemplate = this.oGridViewLevel1;
            gridLevelNode2.LevelTemplate = this.oGridViewLevel2;
            gridLevelNode3.LevelTemplate = this.oGridViewLevel3;
            gridLevelNode3.RelationName = "Level3";
            gridLevelNode2.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode3});
            gridLevelNode2.RelationName = "Level2";
            gridLevelNode1.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode2});
            gridLevelNode1.RelationName = "Level1";
            this.oGridControl.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.oGridControl.Location = new System.Drawing.Point(0, 0);
            this.oGridControl.MainView = this.oGridView;
            this.oGridControl.Name = "oGridControl";
            this.oGridControl.Size = new System.Drawing.Size(429, 299);
            this.oGridControl.TabIndex = 0;
            this.oGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.oGridView,
            this.oGridViewLevel2,
            this.oGridViewLevel3,
            this.oGridViewLevel1});
            // 
            // oGridView
            // 
            this.oGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.level1colId,
            this.level1colName});
            this.oGridView.GridControl = this.oGridControl;
            this.oGridView.Name = "oGridView";
            this.oGridView.MasterRowEmpty += new DevExpress.XtraGrid.Views.Grid.MasterRowEmptyEventHandler(this.oGridView_MasterRowEmpty);
            this.oGridView.MasterRowGetChildList += new DevExpress.XtraGrid.Views.Grid.MasterRowGetChildListEventHandler(this.oGridView_MasterRowGetChildList);
            this.oGridView.MasterRowGetRelationName += new DevExpress.XtraGrid.Views.Grid.MasterRowGetRelationNameEventHandler(this.oGridView_MasterRowGetRelationName);
            this.oGridView.MasterRowGetRelationCount += new DevExpress.XtraGrid.Views.Grid.MasterRowGetRelationCountEventHandler(this.oGridView_MasterRowGetRelationCount);
            // 
            // level1colId
            // 
            this.level1colId.Caption = "Id";
            this.level1colId.FieldName = "Id";
            this.level1colId.Name = "level1colId";
            this.level1colId.Visible = true;
            this.level1colId.VisibleIndex = 0;
            // 
            // level1colName
            // 
            this.level1colName.Caption = "Name";
            this.level1colName.FieldName = "Name";
            this.level1colName.Name = "level1colName";
            this.level1colName.Visible = true;
            this.level1colName.VisibleIndex = 1;
            // 
            // oGridViewLevel2
            // 
            this.oGridViewLevel2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.level3colId,
            this.level3colName});
            this.oGridViewLevel2.GridControl = this.oGridControl;
            this.oGridViewLevel2.Name = "oGridViewLevel2";
            // 
            // level3colId
            // 
            this.level3colId.Caption = "Id";
            this.level3colId.FieldName = "Id";
            this.level3colId.Name = "level3colId";
            this.level3colId.Visible = true;
            this.level3colId.VisibleIndex = 0;
            // 
            // level3colName
            // 
            this.level3colName.Caption = "Name";
            this.level3colName.FieldName = "Name";
            this.level3colName.Name = "level3colName";
            this.level3colName.Visible = true;
            this.level3colName.VisibleIndex = 1;
            // 
            // oGridViewLevel3
            // 
            this.oGridViewLevel3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.level4colId,
            this.level4colName});
            this.oGridViewLevel3.GridControl = this.oGridControl;
            this.oGridViewLevel3.Name = "oGridViewLevel3";
            // 
            // level4colId
            // 
            this.level4colId.Caption = "Id";
            this.level4colId.FieldName = "Id";
            this.level4colId.Name = "level4colId";
            this.level4colId.Visible = true;
            this.level4colId.VisibleIndex = 0;
            // 
            // level4colName
            // 
            this.level4colName.Caption = "Name";
            this.level4colName.FieldName = "Name";
            this.level4colName.Name = "level4colName";
            this.level4colName.Visible = true;
            this.level4colName.VisibleIndex = 1;
            // 
            // Test3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(429, 299);
            this.Controls.Add(this.oGridControl);
            this.Name = "Test3";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Test3";
            ((System.ComponentModel.ISupportInitialize)(this.oGridViewLevel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.oGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.oGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.oGridViewLevel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.oGridViewLevel3)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl oGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView oGridView;
        private DevExpress.XtraGrid.Views.Grid.GridView oGridViewLevel1;
        private DevExpress.XtraGrid.Views.Grid.GridView oGridViewLevel2;
        private DevExpress.XtraGrid.Views.Grid.GridView oGridViewLevel3;
        private DevExpress.XtraGrid.Columns.GridColumn level2colId;
        private DevExpress.XtraGrid.Columns.GridColumn level2colName;
        private DevExpress.XtraGrid.Columns.GridColumn level3colId;
        private DevExpress.XtraGrid.Columns.GridColumn level3colName;
        private DevExpress.XtraGrid.Columns.GridColumn level4colId;
        private DevExpress.XtraGrid.Columns.GridColumn level4colName;
        private DevExpress.XtraGrid.Columns.GridColumn level1colId;
        private DevExpress.XtraGrid.Columns.GridColumn level1colName;
    }
}